# frozen_string_literal: true
require "rails_helper"

RSpec.describe Project, type: :model do
  subject { Project.new(params) }

  let(:params) {
    {
      title: "Some Big Project"
    }
  }

  describe "validations" do
    it "is valid with valid params" do
      expect(subject).to be_valid
    end

    it "requires a title" do
      params[:title] = ""

      expect(subject).to_not be_valid
      expect(subject.errors.keys).to eq [:title]
    end

    it "don't accept a duplicated entry" do
      Project.create title: "Original Project"
      params[:title] = "Original Project"
      expect(subject).to_not be_valid
      expect(subject.errors.keys).to eq [:title]
    end
  end

  describe "soft delete behaviour" do
    it "only access not deleted records" do
      Project.create(params).destroy
      expect(Project.count).to eq 0
      expect(Project.unscoped.count).to eq 1
    end
  end
end
