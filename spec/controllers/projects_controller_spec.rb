# frozen_string_literal: true
require "rails_helper"

RSpec.describe ProjectsController, type: :controller do
  let(:valid_attributes) { { title: "MyString" } }
  let(:valid_session) { {} }

  describe "GET non-existing item" do
    it "redirect to home page and display a message" do
      get :show, { id:  10 }
      expect(flash[:notice]).to eq("Record not found")
    end
  end

  describe "GET index" do
    it "assigns all projects as @projects" do
      project = Project.create! valid_attributes
      get :index, {}, valid_session
      expect(assigns(:projects)).to eq [project]
    end
  end

  describe "GET show" do
    it "assigns the requested project as @project" do
      project = Project.create! valid_attributes
      get :show, { id: project.to_param }, valid_session
      expect(assigns(:project)).to eq project
    end
  end

  describe "GET new" do
    it "assigns a new project as @project" do
      get :new, {}, valid_session
      expect(assigns(:project)).to be_a_new(Project)
    end
  end

  describe "GET edit" do
    it "assigns the requested project as @project" do
      project = Project.create! valid_attributes
      get :edit, { id: project.to_param }, valid_session
      expect(assigns(:project)).to eq project
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Project" do
        expect {
          post :create, { project: valid_attributes }, valid_session
        }.to change(Project, :count).by(1)
      end

      it "assigns a newly created project as @project" do
        post :create, { project: valid_attributes }, valid_session
        expect(assigns(:project)).to be_a(Project)
        expect(assigns(:project)).to be_persisted
      end

      it "redirects to the project" do
        post :create, { project: valid_attributes }, valid_session
        expect(response).to redirect_to(project_path(Project.first))
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved project as @project" do
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(Project).to receive(:save).and_return(false)
        post :create, { project: { title: "invalid value" }},
          valid_session
        expect(assigns(:project)).to be_a_new(Project)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(Project).to receive(:save).and_return(false)
        post :create, { project: { title: "invalid value" }},
          valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested project" do
        project = Project.create! valid_attributes
        # Assuming there are no other projects in the database, this
        # specifies that the Project created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        expect_any_instance_of(Project).to receive(:update).with({ title: "MyString" })

        put :update,
          { id: project.to_param,
            project: { title: "MyString" }},
          valid_session
      end

      it "assigns the requested project as @project" do
        project = Project.create! valid_attributes
        put :update, { id: project.to_param, project: valid_attributes },
          valid_session
        expect(assigns(:project)).to eq project
      end

      it "redirects to the project" do
        project = Project.create! valid_attributes
        put :update, { id: project.to_param, project: valid_attributes },
          valid_session
        expect(response).to redirect_to(project_path(project))
      end
    end

    describe "with invalid params" do
      it "assigns the project as @project" do
        project = Project.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(Project).to receive(:save).and_return(false)
        put :update, { id: project.to_param,
                       project: { title: "invalid value" }},
          valid_session
        expect(assigns(:project)).to eq project
      end

      it "re-renders the 'edit' template" do
        project = Project.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        allow_any_instance_of(Project).to receive(:save).and_return(false)
        put :update, { id: project.to_param,
                      project: { title: "invalid value" }},
          valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested project" do
      project = Project.create! valid_attributes
      expect {
        delete :destroy, { id: project.to_param }, valid_session
      }.to change(Project, :count).by(-1)
    end

    it "redirects to the projects list" do
      project = Project.create! valid_attributes
      delete :destroy, { id: project.to_param }, valid_session
      expect(response).to redirect_to(projects_url)
    end
  end

  describe "DELETE clear" do
    let(:project) { Project.create!(title: "Project") }

    before do
      project.items.create(action: "test")
    end

    it "assigns project" do
      delete :clear, { id: project.to_param }
      expect(assigns(:project)).to eq(project)
    end

    it "does not destroy incomplete items" do
      delete :clear, { id: project.to_param }
      expect(project.items.count).to eq(1)
    end

    it "destroys complete items" do
      project.items.first.update(done: true)
      delete :clear, { id: project.to_param }
      expect(project.reload.items.count).to eq(0)
    end
  end

  describe "JSON Api" do
    # render views to be able to check json results
    render_views

    describe "GET non-existing item" do
      it "redirect to home page and display a message" do
        get :show, id:  10, format: :json

        expect(response).to be_not_found
      end
    end

    describe "GET index" do
      it "list projects without items" do
        project = Project.create! valid_attributes
        get :index, format: :json

        parsed_response = JSON.parse(response.body)

        expect(parsed_response).to_not be_blank
        expect(parsed_response.first["items"]).to be_blank
        expect(parsed_response.first["items"]).to be_kind_of(Array)
      end

      it "list projects with items" do
        project = Project.create! valid_attributes
        project.items.create(action: "todo 1")
        get :index, format: :json

        parsed_response = JSON.parse(response.body)

        expect(parsed_response.first["items"]).to be_kind_of(Array)
        expect(parsed_response.first["items"].first["action"]).to eq("todo 1")
      end
    end

    describe "GET show" do
      it "display project info" do
        project = Project.create! valid_attributes
        get :show, id: project.to_param, format: :json

        parsed_response = JSON.parse(response.body)

        expect(parsed_response).to_not be_nil
        expect(parsed_response.keys).to match_array(%w(id title items url))
      end
    end

    describe "POST create" do
      describe "with valid params" do
        it "creates a new Project" do
          expect {
            post :create, { project: valid_attributes, format: :json }
          }.to change(Project, :count).by(1)
        end

        it "return a success code" do
          post :create, { project: valid_attributes, format: :json }
          expect(response).to be_success
        end

        it "return the project data" do
          post :create, { project: valid_attributes, format: :json }

          parsed_response = JSON.parse(response.body)

          expect(parsed_response).to_not be_nil
          expect(parsed_response["title"]).to eq(valid_attributes[:title])
        end
      end

      describe "with invalid params" do
        it "return an invalid code and error messages structure" do
          allow_any_instance_of(Project).to receive(:save).and_return(false)
          post :create, { project: { title: "invalid value" }, format: :json }

          parsed_response = JSON.parse(response.body)

          expect(response).not_to be_success
          expect(parsed_response).to be_kind_of(Hash)
        end
      end
    end

    describe "PUT update" do
      describe "with valid params" do
        it "updates the requested project" do
          project = Project.create! valid_attributes

          put :update,
            { id: project.to_param,
              project: { title: "MyString" },
              format: :json }
          parsed_response = JSON.parse(response.body)

          expect(parsed_response["title"]).to eq("MyString")
        end

      end

      describe "with invalid params" do
        it "return an invalid code and error messages structure" do
          project = Project.create! valid_attributes
          allow_any_instance_of(Project).to receive(:save).and_return(false)

          put :update, { id: project.to_param,
                         project: { title: "invalid value" },
                         format: :json}
          parsed_response = JSON.parse(response.body)

          expect(response).not_to be_success
          expect(parsed_response).to be_kind_of(Hash)
        end
      end
    end

    describe "DELETE destroy" do
      it "destroys the requested project" do
        project = Project.create! valid_attributes
        expect {
          delete :destroy, { id: project.to_param, format: :json }
        }.to change(Project, :count).by(-1)
      end

      it "don't return any content" do
        project = Project.create! valid_attributes
        delete :destroy, { id: project.to_param, format: :json }
        expect(response).to have_http_status(:no_content)
        expect(response.body).to be_blank
      end
    end

    describe "DELETE clear" do
      let(:project) { Project.create!(title: "Project") }

      before do
        project.items.create(action: "test")
      end

      it "assigns project" do
        delete :clear, { id: project.to_param, format: :json }
        parsed_response = JSON.parse(response.body)

        expect(parsed_response).to_not be_nil
        expect(parsed_response.keys).to match_array(%w(id title items url))
      end

      it "does not destroy incomplete items" do
        delete :clear, { id: project.to_param, format: :json }
        parsed_response = JSON.parse(response.body)

        expect(parsed_response["items"].size).to eq(1)
      end

      it "destroys complete items" do
        project.items.first.update(done: true)
        delete :clear, { id: project.to_param, format: :json }
        parsed_response = JSON.parse(response.body)

        expect(parsed_response["items"].size).to eq(0)
      end
    end
  end
end
