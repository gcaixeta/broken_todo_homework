# frozen_string_literal: true
require "spec_helper"

describe ProjectsReport do
  let(:io_object) { instance_double(IO, :<< => true) }
  let(:params) { { io_out: io_object } }
  let(:project) { Project.create(title: "Project Name") }

  let(:report) do
    ProjectsReport.new(params)
  end

  describe ".new" do
    it "instantiates" do
      expect(report).to be_a(ProjectsReport)
    end
  end

  describe "#execute!" do
    it "prints projects" do
      expect(io_object).to receive(:<<).with("#{project.title}\n")
      report.execute!
    end

    it "prints project items" do
      project.items.create(action: "todo 1")
      expect(io_object).to receive(:<<).with("- [ ] todo 1\n")
      report.execute!
    end

    it "prints project donw items" do
      project.items.create(action: "todo 2", done: true)
      expect(io_object).to receive(:<<).with("- [X] todo 2\n")
      report.execute!
    end
  end
end
