# frozen_string_literal: true
require "rails_helper"

feature "Project page" do
  given!(:project) { Project.create(title: "Project 1") }

  scenario "with no items" do
    visit project_path(project)
    expect(page).to have_content "No items."
    click_on "New Item"
    fill_in "Action", with: "Action 1"
    click_button "Create Item"
    expect(page).to have_content "Action 1"
    expect(page).to have_content "Item was successfully created."
  end

  scenario "clear completed items" do
    visit project_path(project)
    click_on "Clear Completed Items"
    expect(page).to have_content "There are no completed items for this project."
  end
end
