# frozen_string_literal: true
namespace :todo do
  namespace :reports do
    desc "Generate a CSV report of completed Items"
    task csv: :environment do
      CompletedItemsCsvExporter.new.export!
    end

    desc "Print Projects and Items"
    task all: :environment do
      ProjectsReport.new.execute!
    end
  end
end
