# frozen_string_literal: true
Rails.application.routes.draw do
  resources :projects do
    resources :items, except: [:destroy]
    delete "clear", on: :member
  end
  root "projects#index"
end
