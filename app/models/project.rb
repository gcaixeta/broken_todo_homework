# frozen_string_literal: true
class Project < ActiveRecord::Base
  acts_as_paranoid

  validates :title, presence: true, uniqueness: true

  has_many :items, dependent: :destroy
end
