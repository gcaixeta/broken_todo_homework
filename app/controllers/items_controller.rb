# frozen_string_literal: true
class ItemsController < ApplicationController
  before_action :get_project
  before_action :get_item, only: [:edit, :show, :update]

  def index
    respond_to do |format|
      format.csv do
        headers["Content-Disposition"] = "attachment; filename=\"project-#{@project.id}-items.csv\""
        headers["Content-Type"] ||= "text/csv"
        @items = CompletedItem.filter(
          project: @project,
          status: params[:item][:status]
        )
      end
    end
  end

  def new
    @item = @project.items.build
  end

  def create
    @item = @project.items.build(item_params)

    respond_to do |format|
      if @item.save
        format.html { redirect_to project_path(@project),
                      notice: "Item was successfully created."
        }
        format.json { render :show, status: :created, location: [@project, @item] }
      else
        format.html { render "new" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def show
    respond_to do |format|
      format.html { redirect_to @project }
      format.json { render :show, status: :ok, location: [@project, @item] }
    end
  end

  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to project_path(@project),
                      notice: "Item was successfully updated."
        }
        format.json { render :show, status: :ok, location: [@project, @item] }
      else
        format.html { render "edit" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

private

  def get_project
    @project = Project.find(params[:project_id])
  end

  def get_item
    @item = @project.items.find params[:id]
  end

  def item_params
    params.require(:item).permit(:action, :done)
  end
end
