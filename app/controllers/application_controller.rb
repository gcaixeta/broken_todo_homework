# frozen_string_literal: true
require "csv"

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  private

  def record_not_found
    respond_to do |format|
      format.html { redirect_to root_path, notice: "Record not found" }
      format.json {  render json: nil, status: :not_found }
    end
  end
end
