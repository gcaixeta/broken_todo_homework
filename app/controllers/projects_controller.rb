# frozen_string_literal: true
class ProjectsController < ApplicationController
  before_action :set_project, except: [:index, :new, :create]

  def index
    @projects = Project.all
    respond_to do |format|
      format.csv do
        headers["Content-Disposition"] = "attachment; filename=\"projects.csv\""
        headers["Content-Type"] ||= "text/csv"
        @items = CompletedItem.all
      end
      format.html
      format.json
    end
  end

  def show
  end

  def new
    @project = Project.new
  end

  def edit
  end

  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to project_path(@project),
                      notice: "Project was successfully created."
        }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render "new" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to project_path(@project),
                      notice: "Project was successfully updated."
        }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_path,
                    notice: "Project was successfully destroyed."
      }
      format.json { head :no_content }
    end
  end

  def clear
    @project.items.complete.destroy_all
    respond_to do |format|
      format.html { redirect_to project_path(@project),
                    notice: "There are no completed items for this project."
      }
      format.json { render :show, status: :updated, location: @project }
    end
  end

private
  def set_project
    @project = Project.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:title)
  end
end
