# frozen_string_literal: true
class CompletedItem
  def self.all
    Item.complete.order("project_id ASC").includes(:project)
  end

  def self.filter(project:, status: "all")
    items = project.items

    case status
    when "pending"
      items = items.incomplete
    when "done"
      items = items.complete
    end

    items
  end
end
