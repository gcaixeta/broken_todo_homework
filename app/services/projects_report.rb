# frozen_string_literal: true

class ProjectsReport

  def initialize(io_out: STDOUT)
    @io_out = io_out
  end

  def execute!
    Project.all.each do |project|
      io_out << "#{project.title}\n"
      project.items.each do |item|
        io_out << "- [#{item.done ? 'X' : ' '}] #{item.action}\n"
      end
      io_out << "\n"
    end
  end

  private

  attr_reader :io_out
end
