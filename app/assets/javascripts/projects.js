// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(function() {
  $( "#project-items input[type='checkbox']").on( "click", function(event) {
    var itemId = $(event.target).val();
    var itemIsDone = $(event.target).is(":checked");

    var request = $.ajax({
      url: document.location.pathname + "/items/" + itemId + ".json",
      method: "POST",
      data: { item : { done: itemIsDone }, '_method': 'PATCH' },
      dataType: "json"
    }).success(function (data, textStatus, jqXHR) {
      $( "#project-items .item-" + itemId).toggleClass('done');
    });

  });
});
