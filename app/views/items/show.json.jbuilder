# frozen_string_literal: true
json.extract! @item, :id, :action, :project_id, :done, :project_title, :created_at, :updated_at
json.url project_item_url(@project, @item, format: :json)
