# frozen_string_literal: true
json.extract! project, :id, :title, :items
json.url project_url(project, format: :json)
